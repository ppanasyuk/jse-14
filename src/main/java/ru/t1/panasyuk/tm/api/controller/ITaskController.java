package ru.t1.panasyuk.tm.api.controller;

public interface ITaskController {

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void createTask();

    void clearTasks();

    void completeTaskById();

    void completeTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void showTasks();

    void startTaskById();

    void startTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}