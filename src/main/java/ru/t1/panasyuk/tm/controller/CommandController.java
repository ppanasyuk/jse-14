package ru.t1.panasyuk.tm.controller;

import ru.t1.panasyuk.tm.api.controller.ICommandController;
import ru.t1.panasyuk.tm.api.service.ICommandService;
import ru.t1.panasyuk.tm.model.Command;

import static ru.t1.panasyuk.tm.util.FormatUtil.formatBytes;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showErrorArgument() {
        System.out.println("[ERROR]");
        System.err.println("Current program arguments are not correct ...");
        System.exit(1);
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");

        final int processorsCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("PROCESSORS: " + processorsCount);
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    @Override
    public void showErrorCommand() {
        System.out.println("[ERROR]");
        System.err.println("Current command is not correct ...");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.14.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Petr Panasyuk");
        System.out.println("E-mail: ppanasyuk@t1-consulting.ru");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        for (final Command command : commandService.getCommands()) System.out.println(command);
    }

}