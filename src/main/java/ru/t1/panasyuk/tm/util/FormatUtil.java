package ru.t1.panasyuk.tm.util;

import java.text.DecimalFormat;

public final class FormatUtil {

    private static double KILOBYTE = 1024;

    private static double MEGABYTE = KILOBYTE * 1024;

    private static double GIGABYTE = MEGABYTE * 1024;

    private static double TERABYTE = GIGABYTE * 1024;

    private static String NAME_BYTES = "B";

    private static String NAME_BYTES_LONG = "Bytes";

    private static String NAME_KILOBYTE = "KB";

    private static String NAME_MEGABYTE = "MB";

    private static String NAME_GIGABYTE = "GB";

    private static String NAME_TERABYTE = "TB";

    private static String SEPARATOR = " ";

    private static DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    private static String render(final double bytes) {
        return DECIMAL_FORMAT.format(bytes);
    }

    private static String render(final long bytes, final double size) {
        return render(bytes / size);
    }

    private static String render(final long bytes, final String name) {
        return render(bytes) + SEPARATOR + name;
    }

    private static String render(final long bytes, final double size, final String name) {
        return render(bytes, size) + SEPARATOR + name;
    }

    public static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < KILOBYTE)) return render(bytes, NAME_BYTES);
        if ((bytes >= KILOBYTE) && (bytes < MEGABYTE)) return render(bytes, KILOBYTE, NAME_KILOBYTE);
        if ((bytes >= MEGABYTE) && (bytes < GIGABYTE)) return render(bytes, MEGABYTE, NAME_MEGABYTE);
        if ((bytes >= GIGABYTE) && (bytes < TERABYTE)) return render(bytes, GIGABYTE, NAME_GIGABYTE);
        if (bytes >= TERABYTE) return render(bytes, TERABYTE, NAME_TERABYTE);
        return render(bytes, NAME_BYTES_LONG);
    }

    private FormatUtil() {
    }

}